package com.crsystem.crud.CRSystem.repository;

import com.crsystem.crud.CRSystem.model.Representative;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface RepresentativeRepository extends JpaRepository<Representative, Long>,
        QuerydslPredicateExecutor<Representative> {

    Representative findById(long id);




}
