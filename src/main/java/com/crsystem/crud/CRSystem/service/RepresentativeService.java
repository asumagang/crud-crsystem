package com.crsystem.crud.CRSystem.service;

import com.crsystem.crud.CRSystem.model.Representative;
import com.crsystem.crud.CRSystem.repository.RepresentativeRepository;
import com.querydsl.core.BooleanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepresentativeService {

    @Autowired
    private RepresentativeRepository representativeRepository;

    // GET ALL - /users?page=(default = 0) -&firstName -&lastName -&occupation
    public Page<Representative> getAll(Pageable page, String firstName, String lastName, String position){
        BooleanBuilder b = new BooleanBuilder();
        QUser qUser = QUser.user;
        if(firstName != null) {
            b.or(qUser.firstName.toLowerCase().contains(firstName.toLowerCase()));
        }
        if(lastName != null) {
            b.or(qUser.lastName.toLowerCase().contains(lastName.toLowerCase()));
        }
        if(position != null) {
            b.or(qUser.position.toLowerCase().contains(position.toLowerCase()));
        }
        return representativeRepository.findAll(b, page);
    }

}
